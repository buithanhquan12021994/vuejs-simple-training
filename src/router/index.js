import Vue from "vue";
import VueRouter from "vue-router";
import store from '@/store';
import { firebaseApp } from "@/firebase/firebaseInit"
import { isAdmin } from '@/plugins/helpers';
import UserModel from '@/models/User';

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: () => import(/* webpackChunkName: "home" */ '@/views/Home'),
    meta: {
      title: 'Home',
      requiresAuth: false
    }
  },
  {
    path: "/blogs",
    name: "Blogs",
    component: () => import(/* webpackChunkName: "blogs" */'@/views/Blogs'),
    meta: {
      title: 'Blogs',
      requiresAuth: false
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "login" */'@/views/Login'),
    meta: {
      title: 'Login',
      requiresAuth: false
    }
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import(/* webpackChunkName: "register" */'@/views/Register'),
    meta: {
      title: 'Register',
      requiresAuth: false
    }
  },
  {
    path: '/forgot-password',
    name: 'ForgotPassword',
    component: () => import(/* webpackChunkName: "forgot-password" */'@/views/ForgotPassword'),
    meta: {
      title: 'Forgot password',
      requiresAuth: false
    }
  },
  {
    path: '/admin',
    name: 'Admin',
    component: () => import(/* webpackChunkName: "admin" */'@/views/Admin'),
    meta: {
      title: 'Admin',
      requiresAuth: true,
      requireAdmin: true
    }
  },
  {
    path: '/profile',
    name: 'Profile',
    component: () => import(/* webpackChunkName: "profile" */'@/views/Profile'),
    meta: {
      title: 'Profile',
      requiresAuth: true,
    }
  },
  {
    path: '/create-post',
    name: 'CreatePost',
    component: () => import(/* webpackChunkName: "create-post" */'@/views/CreatePost'),
    meta: {
      title: 'Create post',
      requiresAuth: true,
      requireAdmin: true 
    }
  },
  {
    path: '/blog-review',
    name: 'BlogReview',
    component: () => import(/* webpackChunkName: "blog-review" */'@/views/BlogReview'),
    meta: {
      title: 'Blog review',
      requiresAuth: true,
      requireAdmin: true 
    }
  },
  {
    path: '/blog-detail/:blogID',
    name: 'ViewBlog',
    component: () => import(/* webpackChunkName: "view-blog" */'@/views/ViewBlog'),
    meta: {
      title: 'View blog',
      requiresAuth: false 
    }
  },
  {
    path: '/blog-edit/:blogID',
    name: 'EditBlog',
    component: () => import(/* webpackChunkName: "edit-blog" */'@/views/EditBlog'),
    meta: {
      title: 'Edit Blog',
      requiresAuth: true,
      requireAdmin: true 
    }
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  // loading indicator
  store.commit('SET_LOADING_INDICATOR', true);
  document.title = to.meta.title ? to.meta.title : '';
  next();
});

router.beforeEach(async (to, from, next) => {
  const user = firebaseApp.auth().currentUser;
  
  if (to.matched.some((res) => res.meta.requiresAuth)) {
    const userDatabase = await UserModel.findUid(user.uid);
    let admin = false;
    if (typeof userDatabase.data() != 'undefined') {
      admin = isAdmin(userDatabase.data().role);
    }
    if (user) {
      if (to.matched.some((res) => res.meta.requiresAdmin)) {
        if (admin) {
          next();
        }
        return next({ name: 'Home' });
      }
      return next({ ame: 'Home' });
    }
    return next({ name: 'Home' })
  }
  next()
  // let admin = null;
  // if (user) {
  //   let token = await user.getIdTokenResult();
  //   admin = token.claims.role 
  // }
})

router.beforeResolve((to, from ,next) => {
  // disabled loading indicator
  store.commit('SET_LOADING_INDICATOR', false);
  next();
})

export default router;
