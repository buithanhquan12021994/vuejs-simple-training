.create-post {
  position: relative;
  height: 100%;

  .container {
    position: relative;
    height: 100%;
    padding: 10px 25px 60px;
  }

  .error-message {
    background-color: var(--grey400);
    padding: 15px 20px;
    color: var(--white);
    font-size: 14px;
    margin-bottom: 7px;
    border-radius: 8px;
    transition: all .5s ease;
  }

  .invisible {
    opacity: 0;
  }

  button {
    margin-top: 0;
  }

  .router-button {
    text-decoration: none;
    color: var(--white);
  }

  label,
  button,
  .router-button {
    transition: .5s ease-in-out all;
    align-self: center;
    font-size: 14px;
    cursor: pointer;
    border-radius: 20px;
    padding: 12px 24px;
    color: var(--white);
    background-color: var(--grey400);
    text-decoration: none;
    &:hover {
      background-color: rgba(48, 48, 48, 0.7);
    }
  }

  .invisible {
    opacity: 0 !important;
  }

  .err-message {
    width: 100%;
    padding: 12px;
    border-radius: 8px;
    color: var(--white);
    margin-bottom: 10px;
    background-color: var(--grey400);
    opacity: 1;
    transition: .5s ease all;

    p {
      font-size: 14px;
    }

    span {
      font-weight: 600;
    }
  }

  .blog-info {
    display: flex;
    margin-bottom: 32px;

    .input:nth-child(1) {
      min-width: 300px;
    }

    input {
      transition: .5s ease-in-out all;
      padding: 10px 4px;
      border: none;
      border-bottom: 1px solid var(--grey400);
      @media (min-width: 900px) {
        width: 300px;
      }

      &:focus {
        outline: none;
        box-shadow: 0 1px 0 0 var(--grey400);
      }
    }

    .upload-file {
      flex: 1;
      margin-left: 16px;
      position: relative;
      display: flex;
      input {
        display: none;
      }

      .preview {
        margin-right: 16px;
        margin-left: 10px;
        text-transform: initial;
      }
      .lock-btn {
        background-color: var(--disabledButton);
      }
      

      span {
        font-size: 12px;
        margin-left: 16px;
        align-self: center;
      }
    }
  }

  .editor {
    height: 60vh;
    display: flex;
    flex-direction: column;

    .quillWrapper {
      position: relative;
      display: flex;
      flex-direction: column;
      height: 100%;

      #quill-container {
        display: flex;
        flex-direction: column;
        height: 100%;
        overflow-y: auto;
      }
    }

    .ql-editor {
      padding: 20px;
    }
  }

  .blog-actions {
    margin-top: 60px;
    button {
      margin-right: 16px;
    }
  }
}