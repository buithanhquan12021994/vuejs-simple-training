import Vue from 'vue'

// function onClickOutSide({ event, el, handler }) {
//   const isClickOutSide = event.target !== el && !el.contains(event.ta)
//   return isClickOutSide ? handler(event, el) : null;
// }

// function toggleEventListeners(action, eventHandler) {
//   document[`${action}EventListener`]('click', eventHandler, true);
// }

// const instances = new Map();

Vue.directive('click-outside', {
  bind(el, { value }) {
    console.log(el, value)
    // const eventHandler = event => onClickOutSide({ el, event, handler});

    // toggleEventListeners('add', eventHandler);

    // instances.set(el, eventHandler);
  },
  unbind() {
    // const eventHandler = instances.get(el);
    // toggleEventListeners('remove', eventHandler);
    // instances.delete(el);
  },
  stopProp(event) {
    event.stopPropagation();
  }
})