import db from '@/firebase/firebaseInit';

export default class Model {
  constructor() {
    this.collectionDB = null;
  }
}

Model.prototype.initCollection = function() {
  this.collectionDB = db.collection(this.collectionName);
}
