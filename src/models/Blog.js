import Models from './Model';

export default class Blog extends Models {

  constructor({
      blogID = null,
      blogHTML = null,
      blogCoverPhoto = null,
      blogCoverPhotoName = null,
      blogTitle = null, 
      profileId = null,
      blogDate = null
    }= {}) {
    super();
    this.blogID = blogID;
    this.blogHTML = blogHTML;
    this.blogCoverPhoto = blogCoverPhoto;
    this.blogCoverPhotoName = blogCoverPhotoName;
    this.blogTitle = blogTitle;
    this.profileId = profileId;
    this.blogDate = blogDate;

    this.collectionName = 'blog_posts';
    this.initCollection();
  }
  // save for create and update
  async save(uid = null) {
    const timestamp = await Date.now();
    try {
      if (uid != null) {
        await this.collectionDB.doc(uid).set({
          blogHTML: this.blogHTML,
          blogCoverPhoto: this.blogCoverPhoto,
          blogCoverPhotoName: this.blogCoverPhotoName,
          blogTitle: this.blogTitle, 
          profileId: this.profileId,
          date: timestamp,
          dateUpdated: timestamp,
        })
      } else {
        return new Promise((resolve) => {
          let database = this.collectionDB.doc()

          database.set({
            blogID: database.id,
            blogHTML: this.blogHTML,
            blogCoverPhoto: this.blogCoverPhoto,
            blogCoverPhotoName: this.blogCoverPhotoName,
            blogTitle: this.blogTitle, 
            profileId: this.profileId,
            date: timestamp,
            dateUpdated: timestamp,
          })

          resolve(database.id)
        });
      }
    } catch(err) {
      throw new Error(err);
    }
  }
  
  // get 
  async get(limit = null) {
    let response = this.collectionDB.orderBy('date', 'desc');
    if (limit) {
      response = await response.limit(limit).get();
    } else {
      response = await response.get();
    }
    let listPost = [];
    response.forEach(doc => {
      const data = new Blog({
        blogID: doc.id,
        blogHTML: doc.data().blogHTML,
        blogCoverPhoto: doc.data().blogCoverPhoto,
        blogTitle: doc.data().blogTitle,
        blogDate: doc.data().date,
        profileId: doc.data().profileId,
      });
      listPost.push(data);
    });
    return listPost;
  }

  // find
  async find(uid) {
    let instance = await this.collectionDB.doc(uid).get();
    if (typeof instance.data() != 'undefined') {
      return new Blog(instance.data());
    } else {
      return null;
    }
  }

  // delete
  delete(uid) {
    let instance = this.collectionDB.doc(uid);
    return new Promise(resolve => {
      instance.delete()
      .then(() => resolve(true))
      .catch(() => resolve(false))
    })
  }

  // update
  update(uid) {
    const timestamp = Date.now();
    let instance = this.collectionDB.doc(uid);
    return new Promise(resolve => {
      let record = {
        blogHTML: this.blogHTML,
        blogTitle: this.blogTitle,
        blogCoverPhotoName: this.blogCoverPhotoName,
        dateUpdated: timestamp,
      };
      if(this.blogCoverPhoto) {
        record.blogCoverPhoto = this.blogCoverPhoto;
      }

      instance.update(record)
      .then(() => resolve(true))
      .catch(() => resolve(false))
    })
  }
}