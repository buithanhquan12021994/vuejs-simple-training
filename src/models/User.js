import { roles } from '@/plugins/constants';
// import db from '@/firebase/firebaseInit';
import Models from './Model';

export default class User extends Models {

  constructor({
    email = null,
    firstName = null,
    lastName = null,
    username = null,
  } = {}) {
    super();

    this.uid = null;
    this.email = email;
    this.firstName = firstName;
    this.lastName = lastName;
    this.username = username;
    this.role = null;
    this.collectionName = "users";
    
    this.initCollection();
  }

  async create(uid = null) {
    try {
      if (uid != null) {
        await this.collectionDB.doc(uid).set({
          firstName: this.firstName,
          lastName: this.lastName,
          email: this.email,
          username: this.username,
          role: roles.user
        })
      }
    } catch(err) {
      throw new Error(err);
    }
  }

  static async findUid(uid) {
    const self = new User();
    return  await self.collectionDB.doc(uid).get();
  }

  async update(uid = null) {
    await this.collectionDB.doc(uid).update({
      firstName: this.firstName,
      lastName: this.lastName,
      username: this.username,
    })
  }
}

