import firebase from 'firebase';

import 'firebase/firestore';

const firebaseConfig = {
  apiKey: "AIzaSyANoD8tqWqPidh91mrBxNZxmp8RwNyfhac",
  authDomain: "simple-vuejs-blog.firebaseapp.com",
  projectId: "simple-vuejs-blog",
  storageBucket: "simple-vuejs-blog.appspot.com",
  messagingSenderId: "943484278114",
  appId: "1:943484278114:web:0bcfd57eac7f6d6587083b"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const database = firebaseApp.database();
const timestamp = firebase.firestore.FieldValue.serverTimestamp;

export { timestamp, firebaseApp, firebaseConfig, database }
export default firebaseApp.firestore();