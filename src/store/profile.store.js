import { firebaseApp } from '@/firebase/firebaseInit';
// import db from "@/firebase/firebaseInit"; 
import "firebase/auth";
import { getShortName } from '@/plugins/helpers';
import { roles } from '@/plugins/constants';
import { findPropByValObject } from '@/plugins/helpers';
import UserModel from '@/models/User';

const state = {
  id: null,
  user: null,
  admin: null,
  email: null,
  firstName: null,
  lastName: null,
  username: null,
  initials: null,
  shortName: null,
};

const mutations = {
  UPDATE_USER: (state, payload) => {
    state.user = payload;
  },
  SET_PROFILE: (state, doc) => {
    state.id = doc.id;
    state.email = doc.data().email;
    state.firstName = doc.data().firstName;
    state.lastName = doc.data().lastName;
    state.username = doc.data().username;
    let findRole = findPropByValObject(roles, doc.data().role);
    state.admin = findRole == 'admin' ? true : false;
  },
  SET_INITIALS: (state) => {
    state.initials = state.firstName.match(/(\b\S)?/g).join("") + state.lastName.match(/(\b\S)?/g).join("");
    state.shortName = getShortName(state.firstName, state.lastName);
  },
  UPDATE_PROFILE: (state, payload) => {
    const { prop, value } = payload;
    if (state[prop]) {
      state[prop] = value
    }
  }
}

const getters = {
  getProfile:  (state) => ({
    firstName: state.firstName, 
    lastName: state.lastName,
    username: state.username,
    shortName: state.shortName,
    email: state.email,
  }),
  getUser: state => state.user,
  getIsAdmin: state => state.admin,
  getProfileId: state => state.id
}

const actions = {
  async setCurrentUser({ commit }) {
    if (firebaseApp.auth().currentUser.uid) {
      const uid = firebaseApp.auth().currentUser.uid;
      const userDatabase = await UserModel.findUid(uid);
      if (typeof userDatabase.data() != "undefined") {
        commit("SET_PROFILE", userDatabase);
        commit("SET_INITIALS")
        // const token = await user.getIdTokenResult();
        // console.log(token)
      }
    }
  },
 async updateUserSettings({ commit, state }) {
    const user = new UserModel({ username: state.username, firstName: state.firstName, lastName: state.lastName });
    try {
      await user.update(state.id);
      commit("SET_INITIALS");
    } catch (err) {
      console.log(err)
    }
  }
}


export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions
}