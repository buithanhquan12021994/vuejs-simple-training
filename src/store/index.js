import Vue from 'vue'
import Vuex from 'vuex'

import Profile from './profile.store';
import Post from './post.store';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    sampleBlogCards: [
      { blogTitle: 'Blog Card #1', blogCoverPhoto: 'stock-1', blogDate: 'May 1, 2021' },
      { blogTitle: 'Blog Card #2', blogCoverPhoto: 'stock-2', blogDate: 'May 1, 2021' },
      { blogTitle: 'Blog Card #3', blogCoverPhoto: 'stock-3', blogDate: 'May 1, 2021' },
      { blogTitle: 'Blog Card #4', blogCoverPhoto: 'stock-4', blogDate: 'May 1, 2021' },
    ],
    editPost: null,
    loadingIndicator: false,  // loading component for routing
  },
  getters: {
    getLoadingIndicator: state => state.loadingIndicator
  },
  mutations: {
    SET_EDIT_POST: (state, data) => {
      state.editPost = data;
    },
    SET_LOADING_INDICATOR: (state, boolean) => {
      state.loadingIndicator = boolean;
    }
  },
  actions: {
  },
  modules: {
    Profile,
    Post
  }
})
