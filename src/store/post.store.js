import BlogModel from '@/models/Blog';
import rootStore from './index';

const state = {
  blogHTML: "",
  blogTitle: "",
  blogPhotoName: "",
  blogPhotoFileUrl: null,
  blogPhotoReview: false,
  editPost: null,
  blogPosts: [],
  postLoaded: null,
  file: null,
  startLimit: 8
};
const getters = {
  blogPostsFeeds: (state) => state.blogPosts.slice(0,3),
  blogPostsCards: (state) => state.blogPosts.slice(3,7),
  allBlogPosts: state => state.blogPosts
};
const mutations = {
  SET_POST_DETAIL: (state, payload) => {
    let {prop, value} = payload;
    state[prop] = value
  },
  SET_FILE: (state, payload) => {
    state.file = payload
  },
  CREATE_FILE_URL: (state, payload) => {
    state.blogPhotoFileUrl = payload;
  },
  OPEN_PHOTO_URL: (state) => {
    state.blogPhotoReview = !state.blogPhotoReview;
  },
  REFRESH: (state) => {
    state.blogHTML = '';
    state.blogTitle = '';
    state.blogPhotoName = '';
    state.blogPhotoFileUrl = null;
  },
  FILTER_BLOG_POST: (state, blogID) => {
    state.blogPosts = state.blogPosts.filter(i => i.blogID != blogID);
  },
  SET_CURRENT_POST: (state, payload) => {
    const { blogTitle, blogHTML, blogCoverPhoto, blogCoverPhotoName } = payload;
    state.blogHTML = blogHTML;
    state.blogTitle = blogTitle;
    state.blogPhotoName = blogCoverPhotoName;
    state.blogPhotoFileUrl = blogCoverPhoto;
  },
  UPDATE_POST_TO_LIST: (state, payload) => {
    let { blogID, post } = payload;
    let findIndex = state.blogPosts.findIndex(itemPost => itemPost.blogID == blogID);
    if (findIndex > -1) {
      // be found
      state.blogPosts[findIndex] = post;
    }
  },
  PUSH_NEW_POST_TO_LIST: (state, payload) => {
    let { post } = payload;
    state.blogPosts = [
      post,
      ...state.blogPosts
    ]
  },
  SET_MORE_LIMIT: (state, payload) => {
    state.startLimit += payload;
  }
}
const actions = {
  async getPost({state}) {
    try {
      const blog = new BlogModel();
      const result = await blog.get(state.startLimit);
     
      result.forEach(record => {
        if (!state.blogPosts.some(post => post.blogID === record.blogID)) { // check list array have already id or not
          state.blogPosts.push(record);
        }
      })
      this.postLoaded = true;
    } catch (err) {
      console.log(err)
    }
  },
  async deletePost({ commit }, blogID) {
    let blogModel = new BlogModel();
    rootStore.commit('SET_LOADING_INDICATOR', true);
    let removed = await blogModel.delete(blogID);
    if (removed) {
      commit('FILTER_BLOG_POST', blogID);
      rootStore.commit('SET_LOADING_INDICATOR', false);
    } else {
      throw new Error('Fail when remove a record!');
    }
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}